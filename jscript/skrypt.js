document.querySelector('nav').
		querySelector('.button').
		addEventListener('click', zdarzenie => {
			var przycisk= 
				zdarzenie.target.tagName.toLowerCase()==='p' ? 
				zdarzenie.target.parentNode : 
				zdarzenie.target;
			//console.log(przycisk, zdarzenie);
			
			if (!przycisk.hide) {
				/*przycisk.parentNode.style.gridTemplateColumns=`auto ${przycisk.clientWidth}px`;
				przycisk.parentNode.style.width=przycisk.clientWidth+"px";
				setTimeout(() => {
					przycisk.parentNode.style.gridTemplateColumns="0 100%";
					przycisk.children[0].innerText=">";
				}, 1000);*/
				przycisk.parentNode.style.transform="translate3d(-90%,0,0)";
				//przycisk.children[0].innerText=">";
				przycisk.children[0].style.transform="rotate3d(0,0,1,-180deg)";
			}
			else {
				//przycisk.parentNode.style.gridTemplateColumns=`auto ${przycisk.clientWidth}px`;
				przycisk.parentNode.style="";
				przycisk.children[0].style="";
				//przycisk.children[0].innerText="<";
				

			}
			przycisk.hide=!przycisk.hide;
		});

function generujPytania() {
	//ponieramy element naszego menu pytań, w którym będą umieszczone
	var nawigacja = document.querySelector("#poleOdpowiedzi");
	//czyścimy całą zawartość menu, która ewentualnie jeszcze się w nim znajduje
	while(nawigacja.children.length) 
		nawigacja.removeChild(nawigacja.children[nawigacja.children.length-1]);
	//przeskakujemy po wszystkich dodanych pytaniach
	for(let i=0;i<pytaniaOdpowiedzi.length;i++) {
		//tworzymy dowiązania/uchwyt do elementów w dokumencie
		let elem = document.createElement('div');
		let opis = document.createElement('p');
		//podajemy nazwę pytania, pod jaką będzie ono widoczne
		//w naszym menu
		opis.innerText = pytaniaOdpowiedzi[i].nazwa;
		//prawidłową odpiwiedź dokładamy do zmiennej JavaScript elementu HTML
		//dzięki temu uczestnik nie będzie jej widział w kodzie HTML 
		//UWAGA! W fazie produkcji w ogóle ta zmienna nie powinna być 
		//ustawiana a jedynie referencja do pytania z odpowiedzią po stronie 
		//serwera (obecnie nie używamy)
		elem.odp=pytaniaOdpowiedzi[i]['prawidlowa'];
		//dodajmey pola z pytaniem oraz odpowiedziami; te elementy mogą
		//być widoczne w HTML i na stronie
		elem.dataset.pytanie = pytaniaOdpowiedzi[i]['pytanie'];
		elem.dataset.odpa = pytaniaOdpowiedzi[i]['a'];
		elem.dataset.odpb = pytaniaOdpowiedzi[i]['b'];
		elem.dataset.odpc = pytaniaOdpowiedzi[i].c;
		elem.dataset.odpd = pytaniaOdpowiedzi[i].d;
		//do tak utworzonego elementu dodajemy nasłuch zdarzenia onclick
		//dzięki któremu będziemy ładować nasze pytanie do elementu właściwego
		//quizu
		elem.addEventListener('click', zdarzenie => {
			//tworzymy zmienną uchwyt (pomocniczą) dzięki której będziemy
			//mieć pewność, że pracujemy z poprawnym elementem HTML (zawierającym
			//nasze pytanie i odpowiedzi - pola dataset)
			let elem = zdarzenie.target.tagName.toLowerCase()==='p' ? zdarzenie.target.parentNode : zdarzenie.target;
			//pobieramy element z testem
			let sekcja = document.querySelector("main section article");
			//wrzucamy pytatanie
			sekcja.querySelector('h3').innerText = elem.dataset.pytanie;
			sekcja.odp = elem.odp;
			/*funkcja forEach; dzięki niej, bez tworzenia while, for czy do while
				możemy przeskoczyć po każdy elemencie tablicy jaki znalazł się
				w naszym wyniku funkcji querySelectorAll (która wybrała dla nas
				wszystkie pola span znajdujące się w sekcji.
				
				Kolejne elementy tablicy przekazywane są poprzez forEach
				jako zmienna do funkcji anonimowej, jaką sobie wymyślimy
				(w poniższym przykładzie ta zmienna otrzymała nazwę 
				pobranySpan, nazwa, jak zostało wspomniane może byc DOWOLNA)
				w ciele nienazwanej funkcji możemy wykonać z tak przekazanym
				element cokolwiek zechcemy
				*/
			sekcja.querySelectorAll('span').forEach(function(pobranySpan) {
				/*
					poniższy przykład pokazuje użyteczność możliwości odwoływania się
					do właściwości obiektów w stylu tablicowym (gdzie nazwa 
					właściwości nie jest pisana po łączniku - kropce, a występuje jako
					nazwa indeksu w tablicy)
					
					Ponieważ pola inpiut zawierają atrybuty value z kolejnymi 
					odpowiedziami (a,b,c,d), wykorzystamy ten fakt i odpowiednią
					propzycję odpowiedzi przypisamy do span w następujący sposób:
					
					- span w pobranySpan ma swojego rodzica - pole div
					- gdy przejdziemuy do tego rodzica (parentNode)
					  otrzymujemy dostęp do jego wszystkich dzieci (w tym i span)
					- nam jednak potrzebne jest pole input, które w naszym 
					przykładzie jest zawsze jako pierwsze dziecko (indeks 0)
					- odowołujemy się więc do tego dziecka poprzez formułę:
					pobranySpan.parentNode.children[<indeks>]
					
					gdzie <indeks> to wartość od 0 do N (ilość dzieci)
					
					- każde pole input ma wspmnianą wartość a,b,c,d
					- stąd, by pobrać odpowiednią odpowiedź łączymy tą wartość
					z fragmentem tekstu 'odp'
					- tak złączony fragment (np. 'odpa') jest właściwością w tablicy
					dataset naszego elementu - czyli nasza propozycja odpowiedzi
					dla danego indeksu
					*/			
				pobranySpan.innerText=elem.dataset['odp'+pobranySpan.parentNode.children[0].value];
			});
		});
		elem.appendChild(opis);
		nawigacja.appendChild(elem);
	}
}	

document.querySelector('section article div button').
			addEventListener('click', (e) => {
				var sekcja = e.target.parentNode.parentNode;
				var odp = "";
				
				//działanie tego forEach nie jest najefektyczniejsze ze względu na
				//poszukiwanie JEDNEJ odpowiedzi
				//na szczęście przeszukujemy pola radio, które mogą w danej
				//chwili przyjąć tylko jedną wartość
				//document.getElementsByName('odp').forEach((pobranyInput) => {
				sekcja.querySelectorAll('input').forEach((pobranyInput) => {
					if(pobranyInput.checked) {
						odp = pobranyInput.value;
						//ten return będzie miał znaczenie jedynie dla 
						//POJEDYNCZEGO wywołania funkcji, która w danym momencie
						//wywoła dany warunek; nie zmieni to faktu, że pozostałe
						//wywołania forEach i tak się wywołają!
						return;
					}
					//console.log('Wykonanie funkcji forEach dla Input');
				});
				if(odp==="") {
					sekcja.querySelector('#odpowiedz').children[0].innerText=
					"Nie zaznaczyłeś żadnej odpowiedzi!";
				}
				else if (odp!==sekcja.odp) {
					sekcja.querySelector('#odpowiedz').children[0].innerText=
					"Odpowiedź jest nieprawidłowa!";
				}
				else {
					sekcja.querySelector('#odpowiedz').children[0].innerText=
					"Poprawna odpowiedź!";
				}
				//console.log(e.target, sekcja, odp,sekcja.querySelector('#odpowiedz').children[0]);
			});
		
var przerwanieCzas = setInterval(() => {
							if (pytaniaOdpowiedzi) {
								if (przerwanieCzas) 
									clearInterval(przerwanieCzas);
								generujPytania();
							}
						},2000);